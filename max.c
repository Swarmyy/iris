#include "iris.h"

/*Calcul la valeur maximale de la longueur des sépales des iris, Entrée: le tableau des différentes longueurs de sépales,Sortie: retourne la valeur maximale*/

float max(iris *sIris){
    float rtn;
    float max/*,tmpmax*/;
    int i;
    max=sIris->long_sepale[0];
    for(i=0;i<50;i++){
        if(max<sIris->long_sepale[i]){
            max=sIris->long_sepale[i];
        }        
    }
    rtn=max;
    return rtn;
}
