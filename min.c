#include "iris.h"

/*Calcul la valeur minimale de la longueur des sépales des iris, Entrée le tableau des différentes longueurs de sépales,Sortie: retourne la valeur minimale*/

float min(iris *sIris){
    float rtn;
    float min/*,tmpmax*/;
    int i;
    min=sIris->long_sepale[0];
    for(i=0;i<50;i++){
        if(min>sIris->long_sepale[i]){
            min=sIris->long_sepale[i];
        }        
    }
    rtn=min;
    return rtn;
}
