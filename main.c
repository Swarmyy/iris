#include "iris.h"

int main(int argn,char** argv,char** arge){    
    char* nameIris[3]={"Iris Setosa","Iris Versicolor","Iris Virginica"};
    int file_iris;
    float totalLong_sepale[150];
    iris* arrIris[3];
    int i;   
     
    //Définition des erreurs 
    if(argn==1){
        fprintf(stderr,"Un argument requis\n");
    }    

    if(open(argv[1],O_RDONLY)==-1){        
        //problème de paramètre        
        perror("analyse_iris:main:open");
        exit(1);
    }else{
        file_iris=open(argv[1],O_RDONLY);
    }    

    if(read(file_iris,totalLong_sepale,sizeof(float))==-1){
        //pb lect fic
        perror("analyse_iris:main:read");
        exit(2);
    }else{
        read(file_iris,totalLong_sepale,150*sizeof(float));
    }            
    for(i=0;i<3;i++){
        arrIris[i]=stockIris(50*i,50*(1+i),totalLong_sepale);
    }    
    for(i=0;i<3;i++){
        arrIris[i]->moyenne = moyenne(arrIris[i]);
        arrIris[i]->max = max(arrIris[i]);
        arrIris[i]->min = min(arrIris[i]);
        arrIris[i]->specie=nameIris[i];
    }    
    for(i=0;i<3;i++){
        affichage(arrIris[i]);
    }    
    exit(0);
}
