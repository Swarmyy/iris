#include "iris.h"

// Afficher la valeur minimale, la valeur maximale, la moyenne des longueurs, et le nom de l'espèce

void affichage(iris* sIris){
    printf("Espece %s:\n",sIris->specie);
    printf("\tMoyenne:%f\n",sIris->moyenne);
    printf("\tMax:%f\n",sIris->max);
    printf("\tMin:%f\n",sIris->min);
}
