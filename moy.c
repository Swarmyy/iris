#include "iris.h"

/*Calcul la moyenne de la longueur des sépales des iris, Entrée: le tableau des différentes longueurs de sépales,Sortie: retourne la moyenne*/

float moyenne(iris *sIris){
    float rtn;
    int i;
    float sum=0;    
    for(i=0;i<50;i++){
        sum+=sIris->long_sepale[i]/50;
    }
    rtn=sum;
    return rtn;
}
