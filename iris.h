/*
 *authors : MONT Louis & MONTOUCHET Teddy
 *          Groupe 3
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//mettre les defines de cst comme taille mesures,nb d especes...
// Définit la structure des différentes espèces d'iris
typedef struct iris{
    float long_sepale[50];
    float moyenne;
    float max;
    float min;
    char* specie;
}iris;

//Prototype des fonctions 
iris* stockIris(int,int,float*);
float moyenne(iris*);
float max(iris*);
float min(iris*);
void affichage(iris*);
