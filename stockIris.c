#include "iris.h"

/*Fontion qui stock les valeurs des longueurs des iris dans un tableau d'entiers avec en entrée l'indice de début,
l'indice de fin, et le tableau de longueurs de sépales*/

iris* stockIris(int ind_Deb,int ind_Fin,float* totalLongSepale){
    iris *rtnIris;
    rtnIris=malloc(sizeof(iris)); //Allocation mémoire pour la structure d'iris
    int i;
    for(i=0;i<ind_Fin;i++){
        rtnIris->long_sepale[i]=totalLongSepale[ind_Deb+i];
    }
    return rtnIris;
}
